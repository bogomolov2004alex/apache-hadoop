#!/home/linuxbrew/.linuxbrew/bin/python3

from pyspark.sql import SparkSession

def main():
    print("Called main() function")
    spark = SparkSession.builder \
        .appName("ORC Writer") \
        .getOrCreate()

    intermediate_path = "file:///home/barsky/hadoop/jobs/js/temp-output.json"
    output_path = "hdfs://localhost:9000/user/barsky/output/result.orc"

    df = spark.read.json(intermediate_path)

    df.write.format("orc").mode("overwrite").save(output_path)
    
    df.show()

    spark.stop()

#if __name__ == "__main__":
#    main()

main()
print(__name__)
