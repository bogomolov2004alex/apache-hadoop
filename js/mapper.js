#!/home/linuxbrew/.linuxbrew/opt/node@20/bin/node

const fs = require('fs');
const readline = require('readline');
const csv = require('csv-parser');
const AgeOutlinerRemovalModel = require('./model.js');

const input = process.stdin;
const output = process.stdout;

const model = new AgeOutlinerRemovalModel();
const data = [];

const rl = readline.createInterface({
  input: input,
});

input
  .pipe(csv())
  .on('data', (row) => {
    try {
      const filtredRow = model.getObjectWithRemovedOutliners(row);
      
      if (filtredRow !== null) {
        output.write(JSON.stringify(filtredRow) + '\n');
      }
    } catch (err) {
      console.log(err);
    }
  });

