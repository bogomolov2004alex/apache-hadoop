#!/home/linuxbrew/.linuxbrew/opt/node@20/bin/node

class AgeOutlinerRemovalModel {
	constructor() {
		this.targetKey = Object.keys(data[0])[Object.keys(data[0]).length - 2]
		this.data = data.map(entry => parseInt(entry[this.targetKey]));
		this.init();
	}

	init() {
		const sortedData = this.data.sort((a, b) => a - b);
		const q1Index = Math.floor(sortedData.length * 0.25);
		const q3Index = Math.floor(sortedData.length * 0.75);

		this.q1 = sortedData[q1Index];
		this.q3 = sortedData[q3Index];
		this.IQR = this.q3 - this.q1;
		this.lowerBound = this.q1 - 1.5 * this.IQR;
		this.upperBound = this.q3 + 1.5 * this.IQR;
	}

	// Метод для удаления выбросов на основе межквартильного размаха
	getArrayOfRemovedValues(records) {
		return records
			.map(record => parseInt(record[targetKey]))
			.filter(age => age >= this.lowerBound && age <= this.upperBound);
	}

	getObjectWithRemovedOutliners(record) {
		//return records.filter(
		//	record =>
		//		parseInt(record[this.targetKey]) >= this.lowerBound &&
		//		parseInt(record[this.targetKey]) <= this.upperBound
		//);
		if (parseInt(record[this.targetKey]) >= this.lowerBound && parseInt(record[this.targetKey]) <= this.upperBound) {
			return record;
		} else {
			return null;
		}
	}
}

const data = [
	{
		Id: '0',
		Trip_Id: '32244893',
		User_Id: '1470734',
		Sex: 'M',
		Birth_year: '1981',
		Trip_start: '2024-01-31 23:59:33',
		Trip_end: '2024-02-01 00:11:15',
		Origin_Id: '24',
		Destination_Id: '86',
		Age: '10',
		Duration: '0 days 00:11:42',
	},
	{
		Id: '1',
		Trip_Id: '32244892',
		User_Id: '2731702',
		Sex: 'M',
		Birth_year: '1994',
		Trip_start: '2024-01-31 23:59:06',
		Trip_end: '2024-02-01 00:10:49',
		Origin_Id: '48',
		Destination_Id: '279',
		Age: '30',
		Duration: '0 days 00:11:43',
	},
	{
		Id: '2',
		Trip_Id: '32244891',
		User_Id: '1431452',
		Sex: 'M',
		Birth_year: '2001',
		Trip_start: '2024-01-31 23:58:48',
		Trip_end: '2024-02-01 00:01:42',
		Origin_Id: '273',
		Destination_Id: '383',
		Age: '23',
		Duration: '0 days 00:02:54',
	},
	{
		Id: '3',
		Trip_Id: '32244890',
		User_Id: '2312602',
		Sex: 'F',
		Birth_year: '2003',
		Trip_start: '2024-01-31 23:58:44',
		Trip_end: '2024-02-01 00:01:58',
		Origin_Id: '273',
		Destination_Id: '383',
		Age: '21',
		Duration: '0 days 00:03:14',
	},
	{
		Id: '4',
		Trip_Id: '32244889',
		User_Id: '2266427',
		Sex: 'M',
		Birth_year: '1999',
		Trip_start: '2024-01-31 23:58:44',
		Trip_end: '2024-02-01 00:01:39',
		Origin_Id: '273',
		Destination_Id: '383',
		Age: '25',
		Duration: '0 days 00:02:55',
	},
	{
		Id: '5',
		Trip_Id: '32244888',
		User_Id: '1071506',
		Sex: 'M',
		Birth_year: '1964',
		Trip_start: '2024-01-31 23:58:43',
		Trip_end: '2024-02-01 00:23:45',
		Origin_Id: '79',
		Destination_Id: '60',
		Age: '80',
		Duration: '0 days 00:25:02',
	},
	{
		Id: '6',
		Trip_Id: '32244887',
		User_Id: '423423',
		Sex: 'M',
		Birth_year: '1990',
		Trip_start: '2024-01-31 23:58:39',
		Trip_end: '2024-02-01 00:13:57',
		Origin_Id: '160',
		Destination_Id: '354',
		Age: '34',
		Duration: '0 days 00:15:18',
	},
	{
		Id: '7',
		Trip_Id: '32244886',
		User_Id: '2703032',
		Sex: 'M',
		Birth_year: '1970',
		Trip_start: '2024-01-31 23:58:39',
		Trip_end: '2024-02-01 00:24:03',
		Origin_Id: '78',
		Destination_Id: '289',
		Age: '54',
		Duration: '0 days 00:25:24',
	},
	{
		Id: '8',
		Trip_Id: '32244884',
		User_Id: '452007',
		Sex: 'M',
		Birth_year: '1988',
		Trip_start: '2024-01-31 23:58:00',
		Trip_end: '2024-02-01 00:03:35',
		Origin_Id: '193',
		Destination_Id: '68',
		Age: '36',
		Duration: '0 days 00:05:35',
	},
	{
		Id: '9',
		Trip_Id: '32244881',
		User_Id: '11124',
		Sex: 'M',
		Birth_year: '1991',
		Trip_start: '2024-01-31 23:57:28',
		Trip_end: '2024-02-01 00:12:54',
		Origin_Id: '82',
		Destination_Id: '281',
		Age: '33',
		Duration: '0 days 00:15:26',
	},
];

const miniData = [
	{ 'Game Number': '1', ' "Game Length"': ' 39' },
	{ 'Game Number': '2', ' "Game Length"': ' 11' },
	{ 'Game Number': '3', ' "Game Length"': ' 12' },
	{ 'Game Number': '4', ' "Game Length"': ' 53' },
	{ 'Game Number': '5', ' "Game Length"': ' 30' },
	{ 'Game Number': '6', ' "Game Length"': ' 18' },
	{ 'Game Number': '7', ' "Game Length"': ' 33' },
	{ 'Game Number': '8', ' "Game Length"': ' 13' },
	{ 'Game Number': '9', ' "Game Length"': ' 117' },
	{ 'Game Number': '10', ' "Game Length"': ' 9' },
];

module.exports = AgeOutlinerRemovalModel;

