#!/home/linuxbrew/.linuxbrew/opt/node@20/bin/node

const fs = require('fs');
const readline = require('readline');
const { exec } = require('child_process');

const intermediateFile = '/home/barsky/hadoop/jobs/js/temp-output.json';

const rl = readline.createInterface({
	input: process.stdin,
});

const ageCounts = {};

rl.on('line', line => {
	try {
		const record = JSON.parse(line);
		const age = parseInt(
			record[Object.keys(record)[Object.keys(record).length - 2]]
		);

		if (!isNaN(age)) {
			if (ageCounts[age]) {
				++ageCounts[age];
			} else {
				ageCounts[age] = 1;
			}
		}
	} catch (err) {
		console.error('Error processing line: ', err);
	}
});

rl.on('close', async () => {
	try {
		const writer = fs.createWriteStream(intermediateFile);

		for (const age in ageCounts) {
			writer.write(JSON.stringify({ Age: age, Count: ageCounts[age] }) + '\n');
			console.log(`${age}\t${ageCounts[age]}`);
		}

		writer.end(() => {
			console.log(
				'Reducer finished successfully and saved data in JSON to',
				intermediateFile
			);
			runSparkJob();
		});
	} catch (err) {
		console.error('Error writing output: ', err);
	}
});

const runSparkJob = () => {
	console.log('Starting Spark job...');

	const env = {
		...process.env,
		PYTHONPATH: `/home/barsky/spark/spark-3.5.1-bin-hadoop3/python:/home/barsky/spark/spark-3.5.1-bin-hadoop3/python/lib/py4j-0.10.9.7-src.zip`,
	};

	exec(
		`python3 /home/barsky/hadoop/jobs/py/spark_orc_writer.py`,
		{ env },
		(error, stdout, stderr) => {
			if (error) {
				console.log(`exec error: ${error}`);
				return;
			}
			console.log(`stdout: ${stdout}`);
			console.log(`stderr: ${stderr}`);
		}
	);
};
